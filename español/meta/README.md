# Integración con Meta 
Somos una compañía pequeña en fase de startup, y mientras desarrollemos la app, Facebook requiere un proceso más largo que lo normal - pero de gran beneficio - para conectar tu página de Facebook con nuestra plataforma. 

Este gran beneficio para tu empresa se trata de activar el centro de negocios de Facebook a tu compañía, si aún no lo tienes, dándote más herramientas y seguridad para tus cuentas de Meta, por ejemplo, Instagram, y luego activar el centro de developers.

Además, este proceso es la única manera de poder integrar un chatbot con Whatsapp (y tenemos uno en desarrollo!), abriéndote el camino para tu negocio aprovechar de las mejores herramientas de inteligencia artificial que existen hoy en día.

Una vez aprobada nuestra app por Facebook, el proceso será mucho más fácil.

Nosotros también ofrecemos un servicio para ayudarte a activar estas herramientas. No dudes en ponerte en contacto con nosotros! Estamos para servirte.
